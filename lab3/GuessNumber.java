import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		int guess=-100;
		int attempt=-1;
		do {
			guess = reader.nextInt(); //Read the user input
			attempt++;
			if (guess == number) {
				System.out.print("Congratulations! You won after "+ attempt+" attempts!" );
			} else if(guess<number) {
				System.out.print("Sorry!\n" + "Mine is greater than your guess.\n" + "Type -1 to quit or guess another:");
				if (guess == -1) {
					System.out.print("\nSorry, the number was " + number);
					break;
				}
			}
			else {
				System.out.print("Sorry!\n" + "Mine is lower than your guess.\n" + "Type -1 to quit or guess another:");
				if (guess == -1) {
					System.out.print("\nSorry, the number was " + number);
					break;
				}
			}
		}while (guess!=number);
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}