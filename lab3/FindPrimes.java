import java.io.IOException;
import java.util.Scanner;
public class FindPrimes {
    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        int number =reader.nextInt();//Read the user input
        for(int i=2;i<=number;i++){
            if(isPrime(i)){
                System.out.print(i+",");
            }
        }
    }
    public static boolean isPrime(int x){

        for(int i=2; i<=Math.sqrt(x); i++){
            if(x%i == 0) {
                return false;
            }
        }
        return true;
    }
}
