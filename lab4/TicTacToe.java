import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

		printBoard(board);
		while (!IsFull(board)) {
			while (true) {
				System.out.print("Player 1 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				int col = reader.nextInt();
				if (row < 1 || col < 1 || row > 3 || col > 3)
					System.out.println("This position is out of the board! Try again.");
				else if (board[row - 1][col - 1] != ' ')
					System.out.println("This spot is already full! Try again.");
				else {
					board[row - 1][col - 1] = 'X';
					printBoard(board);
					break;
				}
			}
			if(IsFull(board)) {
				if(!checkBoard(board))
					System.out.println("Game is over! It's a draw.");
				break;
			}
			if(checkBoard(board)) {
				System.out.println("Game is over! Player 1 has won.");
				break;
			}

			while (true) {
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();
				if (row < 1 || col < 1 || row > 3 || col > 3)
					System.out.println("This position is out of the board! Try again.");
				else if (board[row - 1][col - 1] != ' ')
					System.out.println("This spot is already full! Try again.");
				else {
					board[row - 1][col - 1] = 'O';
					printBoard(board);
					break;
				}
			}
			if(checkBoard(board)){
				System.out.println("Game is over! Player 2 has won.");
				break;
			}
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
	public static boolean IsFull(char[][] board) {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(board[i][j] == ' ') {
					return false;
				}
			}
		}
		return true;
	}
	public static boolean checkBoard(char[][] board) {
		for(int i = 0; i < 3; i++) {
			if(board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ') {
				return true;
			}
		}
		for(int j = 0; j < 3; j++) {
			if(board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != ' ') {
				return true;
			}
		}
		if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ') {
			return true;
		}
		if(board[2][0] == board[1][1] && board[1][1] ==  board[0][2] && board[2][0] != ' ') {
			return true;
		}

		return false;
	}
}
/*public class TicTacToe { //Exercise 1
*//*
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

		printBoard(board);
		while (!IsFull(board)) {
			while (true) {
				System.out.print("Player 1 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				int col = reader.nextInt();
				if (row < 1 || col < 1 || row > 3 || col > 3)
					System.out.println("This position is out of the board! Try again.");
				else if (board[row - 1][col - 1] != ' ')
					System.out.println("This spot is already full! Try again.");
				else {
					board[row - 1][col - 1] = 'X';
					printBoard(board);
					break;
				}
			}
			if(IsFull(board)) break;
			while (true) {
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();
				if (row < 1 || col < 1 || row > 3 || col > 3)
					System.out.println("This position is out of the board! Try again.");
				else if (board[row - 1][col - 1] != ' ')
					System.out.println("This spot is already full! Try again.");
				else {
					board[row - 1][col - 1] = 'O';
					printBoard(board);
					break;
				}
			}
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
	public static boolean IsFull(char[][] board) {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(board[i][j] == ' ') {
					return false;
				}
			}
		}
		return true;
	}*//*
}*/


