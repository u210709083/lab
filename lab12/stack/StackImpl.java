package stack;


public class StackImpl extends StackItem implements Stack{
    private StackItem top;
    @Override
    public void push(Object item) {
        StackItem temp= new StackItem(item);
        temp.setNext(top);
        top=temp;
    }

    @Override
    public Object pop() {
        if (top != null) {
            Object item = top.getItem();
            top = top.getNext();
            return item;
        }
        return null;
    }

    @Override
    public boolean empty() {
        return top== null;
    }
}
