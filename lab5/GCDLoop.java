
public class GCDLoop {
    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        System.out.println(gcd(a,b));

    }

    private static int gcd(int a, int b){
            int r=1;
            if(b==0) return a;
            if(a<0) a=-a;
            if(b<0) b=-b;
            while(a%b>0){
                r=a%b;
                a=b;
                b=r;
            }
            return b;
    }

}
