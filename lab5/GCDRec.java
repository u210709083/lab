public class GCDRec {
    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        System.out.println(gcd(a,b));
    }

    private static int gcd(int a, int b){

        if(b==0) return a;
        if(a<0) a=-a;
        if(b<0) b=-b;

        return gcd(b, a%b);
    }
}
