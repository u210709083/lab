package generics;

public class Test {
    public static void main(String[] args) {
        testStack(new StackImpl<Number>());
    }

    public static void testStack(Stack<Number> stack){
        stack.push(3);
        stack.push(9);
        stack.push(5);
        stack.push(3.14);
        System.out.println("Stack_1" + stack.toList());

        Stack<Integer> stack2 = new StackImpl<>();
        stack2.push(10);
        stack2.push(20);
        stack2.push(30);
        System.out.println("Stack_2" + stack2.toList());

        stack.addAll(stack2);
        System.out.println("Stack_1" + stack.toList());

        while(!stack.empty()){
            System.out.println(stack.pop());
        }
        System.out.println(stack.toList());
    }
}



