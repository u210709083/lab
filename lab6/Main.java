public class Main {
    public static void main(String[] args){

        Rectangle rect = new Rectangle(3,5,1,2);
        System.out.println("Area of rectangle:"+ rect.area());
        System.out.println("Perimeter of rectangle:"+ rect.perimeter());
        rect.corners();

        Circle c1=new Circle();
        c1.radius=10;
        System.out.println("Area of circle:"+ c1.area());
        c1.area();
        System.out.println("Perimeter of circle:"+ c1.perimeter());
        c1.perimeter();
        Circle c2=new Circle(3,9,9);
        c1.intersect(c2);
    }
}
