public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;

    public Rectangle(){
        sideA=1;
        sideB=1;
        topLeft=new Point();
    }

    public Rectangle(int x, int y, int xCord, int yCord){
        sideA = x;
        sideB = y;
        topLeft=new Point(xCord,yCord);
    }

    public int area(){
        return sideA*sideB;
    }
    public int perimeter(){
        return (sideA+sideB)*2;
    }
    public void corners(){
        Point a=topLeft;
        Point b=new Point(topLeft.xCoord, topLeft.yCoord-sideA);
        Point c=new Point(topLeft.xCoord+sideB, topLeft.yCoord-sideA );
        Point d=new Point(topLeft.xCoord+sideB, topLeft.yCoord );
        System.out.print("Corners:"+"\n"
                +a.xCoord + ", " + a.yCoord + " \n"
                + b.xCoord + ", " + b.yCoord + "\n"
                + c.xCoord + ", " + c.yCoord + " \n"
                + d.xCoord + ", " + d.yCoord + "\n");
    }
}
