public class Circle {
    int radius;
    Point center;

    public Circle(){
        radius=1;
        center=new Point();
    }

    public Circle(int r, int xCord, int yCord){
        radius = r;
        center=new Point(xCord,yCord);
    }

    public double area(){
        return Math.PI*Math.pow(radius,2);
    }
    public double perimeter(){
        return Math.PI*radius*2;
    }

    public boolean intersect(Circle a){
        double dSquare= Math.pow((a.center.xCoord-this.center.xCoord),2)+Math.pow((a.center.yCoord-this.center.yCoord),2);
        double rdSquare= Math.pow((a.radius+ this.radius),2);
        if(rdSquare>dSquare) {
            System.out.println("These circles have an intersection");
            return true;
        }
        System.out.println("These circles do not have any intersection");
        return false;
    }
}
