public class Point {
    int xCoord;
    int yCoord;

    public Point(){
        xCoord=0;
        yCoord=0;
    }

    public Point(int x, int y){
        xCoord = x;
        yCoord = y;
    }
}
