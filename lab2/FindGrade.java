public class FindGrade {
    public static void main(String[] args){
        int numGrade = Integer.parseInt(args[0]);
        String letterGrade;
        if (numGrade>=90&&numGrade<=100)
            letterGrade="A";
        else if (numGrade>=80&&numGrade<90)
            letterGrade="B";
        else if (numGrade>=70&&numGrade<80)
            letterGrade="C";
        else if (numGrade>=60&&numGrade<70)
            letterGrade="D";
        else if (numGrade>=0&&numGrade<60)
            letterGrade="F";
        else
            letterGrade="It is not a valid score!";

        System.out.println(letterGrade);
    }

}

