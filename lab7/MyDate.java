public class MyDate {
    private int day, month, year;

    public MyDate(int day,int month,int year){
        this.day=day;
        this.month=month;
        this.year=year;
    }
    public static int lastDayOfMonth(MyDate date) {
        int result = 31;
        if (date.month == 2) result = (date.year % 4 == 0) ? 29 : 28;
        if (date.month == 4 || date.month == 6 || date.month == 8 || date.month == 11) result = 30;
        return result;
    }

    public void incrementDay(){
        day+=1;
        if(day>lastDayOfMonth(this)){
            day=1;
            month++;
        }
        if (month==13){
            month=1;
            year++;
        }
    }
    public void decrementDay(){
        if(day==1 && month==1){
            day=31;
            month=12;
            year--;
        }
        else if(day==1){
            month--;
            day=lastDayOfMonth(this);
        }
        else day-=1;
    }
    public void incrementMonth(){
        month=month%12+1;
        if(month==1) year++;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void decrementMonth(){
        if(month==1) {
            month=12;
            year--;
        }
        else month--;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void incrementYear(){
        year++;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void decrementYear(){
        year--;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void incrementYear(int y){
        year=year+y;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void decrementYear(int y){
        year=year-y;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void incrementMonth(int m){
        year=year+(month+m)/12;
        month=(month+m)%12;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void decrementMonth(int m){
        while ((month-m)<0){
            year--;
            m=m-12;
        }
        month=(month+12-m)%12;
        while (day>lastDayOfMonth(this))
            day--;
    }
    public void incrementDay(int d){
        for (int i = 0; i <d ; i++) {
            incrementDay();
        }
    }
    public void decrementDay(int d){
        for (int i = 0; i < d ; i++) {
            decrementDay();
        }
    }
    public String toString(){
        if(month<10 && day>9) return year+"-"+"0"+month+"-"+day;
        if(month>9 && day<10) return year+"-"+month+"-"+"0"+day;
        if(month<9 && day<10) return year+"-"+"0"+month+"-"+"0"+day;
        return year+"-"+month+"-"+day;
    }
    public boolean isBefore(MyDate dt){
        if(dt.year>this.year)
            return true;
        else if(dt.year==this.year && dt.month>this.month)
            return true;
        else if (dt.year==this.year && dt.month==this.month&& dt.day>this.day)
            return true;
        return false;
    }
    public boolean isAfter(MyDate dt){
        if(dt.year<this.year)
            return true;
        else if(dt.year==this.year && dt.month<this.month)
            return true;
        else if (dt.year==this.year && dt.month==this.month&& dt.day<this.day)
            return true;
        return false;
    }
    public int dayDifference(MyDate dt){
        int diff=0;
        MyDate temp=new MyDate(this.day,this.month,this.year);
        while (temp.isAfter(dt)){
            temp.decrementDay();
            diff++;
        }
        while (temp.isBefore(dt)){
            temp.incrementDay();
            diff++;
        }
        return diff;
    }
}
