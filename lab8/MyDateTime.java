public class MyDateTime extends Object{

    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return date + " " + time;

    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0){
            date.decrementDay(-dayDiff);
        }else{
            date.incrementDay(dayDiff);
        }

    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff){
        int dayDiff=time.incrementMinute(diff);
        if (dayDiff < 0){
            date.decrementDay(-dayDiff);
        }else{
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff){
        incrementMinute(-diff);
    }

    public void incrementYear(int diff){
        date.incrementYear(diff);
    }
    public void decrementDay(){
        date.decrementDay();
    }
    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementMonth(){
        date.decrementMonth();
    }
    public void incrementDay(int i) {
        date.incrementDay(i);
    }
    public void incrementMonth(int diff){
        date.incrementMonth(diff);
    }
    public void decrementMonth(int diff){
        incrementMonth(-diff);
    }
    public void decrementDay(int diff) {
        for (int i = 0; i <diff ; i++) {
            date.decrementDay();
        }
    }
    public void decrementYear(int diff){
        incrementYear(-diff);
    }
    public void incrementMonth(){
        incrementMonth(1);
    }
    public void incrementYear(){
        incrementYear(1);
    }
    public boolean isBefore(MyDateTime anotherDate) {
        String strA =toString().replaceAll("-", "");
        strA=strA.replaceAll(":","");
        strA=strA.replaceAll(" ","");
        String strB=anotherDate.toString().replaceAll("-", "");
        strB=strB.replaceAll(":","");
        strB=strB.replaceAll(" ","");
        long a = Long.parseLong(strA) ;
        long b = Long.parseLong(strB);
        return a < b;
    }

    public boolean isAfter(MyDateTime anotherDate) {
        String strA =toString().replaceAll("-", "");
        strA=strA.replaceAll(":","");
        strA=strA.replaceAll(" ","");
        String strB=anotherDate.toString().replaceAll("-", "");
        strB=strB.replaceAll(":","");
        strB=strB.replaceAll(" ","");
        long a = Long.parseLong(strA) ;
        long b = Long.parseLong(strB);
        return a > b;
    }
    public String dayTimeDifference(MyDateTime anotherDate) {
        int dayDiff = 0;
        int hourDiff = 0;
        int minDiff = 0;

        if (isBefore(anotherDate)){
            MyDateTime dtTm = new MyDateTime(date, time); //a copy of current object
            MyTime time1 = new MyTime(dtTm.time.hour,dtTm.time.minute);
            MyTime time2= new MyTime(anotherDate.time.hour, anotherDate.time.minute);
            while (dtTm.isBefore(anotherDate)){
                if(dtTm.date != anotherDate.date){
                    dtTm.incrementDay();
                    dayDiff++;
                }else{
                    while(time1.isBefore(time2)){
                        if(time1.hour != time2.hour){
                            dtTm.incrementHour();
                            hourDiff++;
                        }else{
                            dtTm.incrementMinute(1);
                            minDiff++;
                        }
                }
            }
            }
        }
        else if(isAfter(anotherDate)){
            MyDateTime dtTm = new MyDateTime(date, time); //a copy of current object
            MyTime time1 = new MyTime(dtTm.time.hour,dtTm.time.minute);
            MyTime time2= new MyTime(anotherDate.time.hour, anotherDate.time.minute);
            while (dtTm.isAfter(anotherDate)){
                if(dtTm.date == anotherDate.date){
                    while(time1.isAfter(time2)){
                        if(time1.hour == time2.hour){
                            time2.incrementMinute(1);
                            minDiff++;
                        }else{
                            dtTm.incrementHour(-1);
                            hourDiff++;
                        }
                    }
                }else{
                    dtTm.incrementDay(-1);
                    dayDiff++;
                }
            }
        }
        String str=dayDiff+" day(s)"+hourDiff+" hour(s)"+minDiff+" minuste(s)";
        return str;
    }
}







